﻿using MySql.Data.MySqlClient;
using PowerLine.CapaDatos;
using PymeSearch.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace PymeSearch.Data
{
    public class UsuarioDatos
    {
        public static Usuario getUserLogin(string pUsuario)
        {
            Usuario user = new Usuario();

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ConnectMySQL");
                MySqlCommand command = new MySqlCommand("SP_PS_User_SelectLogin");
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@TC_UserName", pUsuario);
                DataSet ds = db.ExecuteReader(command, "tps_user");

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    user.usuario = row["TC_UserName"].ToString();
                    user.contrasenna = row["TC_Password"].ToString();
                    //user.tipoUsuario = Convert.ToInt32(row["TF_UserType"].ToString());
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return user;
        }

        public static bool createUser(Usuario pUsuario)
        {
            bool resp = false;

            try
            {
                Database db = DatabaseFactory.CreateDatabase("ConnectMySQL");
                MySqlCommand command = new MySqlCommand("SP_PS_User_Insert");
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@TC_UserName", pUsuario.usuario);
                command.Parameters.AddWithValue("@TC_Password", pUsuario.contrasenna);
                command.Parameters.AddWithValue("@TN_UserType", 1);
                DataSet ds = db.ExecuteReader(command, "tps_user");
                resp = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return resp;
        }
    }
}