﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Principal.aspx.cs" Inherits="PymeSearch.Views.Principal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <!-- Basic Page Needs
================================================== -->
    <title>PymeSearch</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <!-- CSS
================================================== -->
    <script src="../Assets/templateStyle/scripts/jquery-3.5.1.min.js"></script>
    <link href="../Assets/templateStyle/css/style.css" rel="stylesheet" />
    <link href="../Assets/templateStyle/css/main-color.css" rel="stylesheet" />
    <script src="https://unpkg.com/leaflet@1.0.2/dist/leaflet.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.2/dist/leaflet.css" />
    <script src="../Assets/Js/Principal.js"></script>
    <script src="../Assets/templateStyle/sweetalert.min.js"></script>
</head>

<body>

    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Header Container
================================================== -->
        <header id="header-container">

            <!-- Header -->
            <div id="header">
                <div class="container">

                    <!-- Left Side Content -->
                    <div class="left-side">

                        <!-- Logo -->
                        <div id="logo">
                            <%--<a href="index.html">
                            <img src="images/logo.png" alt=""></a>--%>
                            <h5>PymeSearch</h5>
                        </div>

                        <!-- Mobile Navigation -->
                        <div class="mmenu-trigger">
                            <button class="hamburger hamburger--collapse" type="button">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                    <div class="right-side">
                        <div class="header-widget">
                            <a href="#sign-in-dialog" class="sign-in popup-with-zoom-anim"><i class="sl sl-icon-login"></i>Ingresar/Registrar comercio</a>
                        </div>
                    </div>
                    <div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">

                        <div class="small-dialog-header">
                            <h3>Ingresar/Registrar comercio</h3>
                        </div>
                        <div class="sign-in-form style-1">

                            <ul class="tabs-nav">
                                <li class=""><a href="#tab1">Ingresar</a></li>
                                <li><a href="#tab2">Registrar comercio</a></li>
                            </ul>

                            <div class="tabs-container alt">
                                <div class="tab-content" id="tab1" style="display: none;">
                                    <!-- Login -->
                                    <form method="post" class="login" action="ListadoOrdenes.aspx" onsubmit="return APP.Principal.iniciarSesion();" id="Ingresar">

                                        <p class="form-row form-row-wide">
                                            <label for="username">
                                                Correo electrónico:
										<i class="im im-icon-Male"></i>
                                                <input type="text" class="input-text" name="username" id="username" required="required" />
                                            </label>
                                        </p>

                                        <p class="form-row form-row-wide">
                                            <label for="password">
                                                Contraseña:
										<i class="im im-icon-Lock-2"></i>
                                                <input class="input-text" type="password" name="password" id="password" required="required" />
                                            </label>
                                            <span class="lost_password">
                                                <a href="#">Olvidó su contraseña?</a>
                                            </span>
                                        </p>

                                        <div class="form-row">
                                            <input type="submit" class="button border margin-top-5" name="login" value="Iniciar sesión" onclick="APP.Principal.iniciarSesion();" />
                                        </div>

                                    </form>
                                </div>
                                <!-- Register -->
                                <div class="tab-content" id="tab2" style="display: none;">

                                    <form method="post" class="register">

                                        <p class="form-row form-row-wide">
                                            <label for="email2">
                                                Correo electrónico:
									    <i class="im im-icon-Mail"></i>
                                                <input type="email" class="input-text" name="email" id="userEmail" value="" required="required" />
                                            </label>
                                        </p>

                                        <p class="form-row form-row-wide">
                                            <label for="password1">
                                                Contraseña:
									    <i class="im im-icon-Lock-2"></i>
                                                <input class="input-text" type="password" name="password1" id="userPassword" required="required" />
                                            </label>
                                        </p>

                                        <input type="submit" class="button border fw margin-top-10" name="register" value="Registarme" onclick="APP.Principal.CrearUsuarioComercio();" />

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="clearfix"></div>
        <!-- Banner
================================================== -->
        <div id="map-container" class="fullwidth-home-map">

            <div id="map">
            </div>

            <div class="main-search-inner">

                <div class="container">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="main-search-input">

                                <div class="main-search-input-item">
                                    <input type="text" id="txtBusqueda" placeholder="Qué estás buscando?" />
                                </div>                                

<%--                                <div class="main-search-input-item">
                                    <select data-placeholder="All Categories" class="chosen-select">
                                        <option>All Categories</option>
                                        <option>Shops</option>
                                        <option>Hotels</option>
                                        <option>Restaurants</option>
                                        <option>Fitness</option>
                                        <option>Events</option>
                                    </select>
                                </div>--%>

                                <button class="button" type="submit" onclick="APP.Principal.realizarBusqueda()">Search</button>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>



        <!-- Content
================================================== -->
        <section class="fullwidth margin-top-0 padding-top-0 padding-bottom-40" data-background-color="#fcfcfc">
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <h3 class="headline margin-top-75">
                            <strong class="headline-with-separator">Popular Categories</strong>
                        </h3>
                    </div>

                    <div class="col-md-12">
                        <div class="categories-boxes-container-alt margin-top-5 margin-bottom-30">

                            <!-- Box -->
                            <a href="listings-list-with-sidebar.html" class="category-small-box-alt">
                                <i class="im im-icon-Hamburger"></i>
                                <h4>Eat & Drink</h4>
                                <span class="category-box-counter-alt">12</span>
                                <img src="images/category-box-01.jpg"/>
                            </a>

                            <!-- Box -->
                            <a href="listings-list-with-sidebar.html" class="category-small-box-alt">
                                <i class="im  im-icon-Sleeping"></i>
                                <h4>Hotels</h4>
                                <span class="category-box-counter-alt">32</span>
                                <img src="images/category-box-02.jpg"/>
                            </a>

                            <!-- Box -->
                            <a href="listings-list-with-sidebar.html" class="category-small-box-alt">
                                <i class="im im-icon-Shopping-Bag"></i>
                                <h4>Shops</h4>
                                <span class="category-box-counter-alt">11</span>
                                <img src="images/category-box-03.jpg"/>
                            </a>

                            <!-- Box -->
                            <a href="listings-list-with-sidebar.html" class="category-small-box-alt">
                                <i class="im im-icon-Cocktail"></i>
                                <h4>Nightlife</h4>
                                <span class="category-box-counter-alt">15</span>
                                <img src="images/category-box-04.jpg"/>
                            </a>

                            <!-- Box -->
                            <a href="listings-list-with-sidebar.html" class="category-small-box-alt">
                                <i class="im im-icon-Electric-Guitar"></i>
                                <h4>Events</h4>
                                <span class="category-box-counter-alt">21</span>
                                <img src="images/category-box-05.jpg"/>
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer
================================================== -->
        <div class="copyrights">© 2019 Listeo. All Rights Reserved.</div>
        <!-- Footer / End -->

    </div>
    <!-- Scripts
================================================== -->

    <script src="../Assets/templateStyle/scripts/jquery-migrate-3.3.1.min.js"></script>
    <script src="../Assets/templateStyle/scripts/mmenu.min.js"></script>
    <script src="../Assets/templateStyle/scripts/chosen.min.js"></script>
    <script src="../Assets/templateStyle/scripts/slick.min.js"></script>
    <script src="../Assets/templateStyle/scripts/rangeslider.min.js"></script>
    <script src="../Assets/templateStyle/scripts/magnific-popup.min.js"></script>
    <script src="../Assets/templateStyle/scripts/waypoints.min.js"></script>
    <script src="../Assets/templateStyle/scripts/counterup.min.js"></script>
    <script src="../Assets/templateStyle/scripts/jquery-ui.min.js"></script>
    <script src="../Assets/templateStyle/scripts/tooltips.min.js"></script>
    <script src="../Assets/templateStyle/scripts/custom.js"></script>
    <!-- Google Autocomplete -->
    <script>
        function initAutocomplete() {
            var input = document.getElementById('autocomplete-input');
            var autocomplete = new google.maps.places.Autocomplete(input);

            autocomplete.addListener('place_changed', function () {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    return;
                }
            });

            if ($('.main-search-input-item')[0]) {
                setTimeout(function () {
                    $(".pac-container").prependTo("#autocomplete-container");
                }, 300);
            }
        }

    </script>

    <script src="../Assets/templateStyle/scripts/infobox.min.js"></script>
    <script src="../Assets/templateStyle/scripts/leaflet-markercluster.min.js"></script>


    <!-- Style Switcher
================================================== -->
    <script src="../Assets/templateStyle/scripts/switcher.js"></script>
    <div id="style-switcher">
        <h2>Color Switcher <a href="#"><i class="sl sl-icon-settings"></i></a></h2>

        <div>
            <ul class="colors" id="color1">
                <li><a href="#" class="main" title="Main"></a></li>
                <li><a href="#" class="blue" title="Blue"></a></li>
                <li><a href="#" class="green" title="Green"></a></li>
                <li><a href="#" class="orange" title="Orange"></a></li>
                <li><a href="#" class="navy" title="Navy"></a></li>
                <li><a href="#" class="yellow" title="Yellow"></a></li>
                <li><a href="#" class="peach" title="Peach"></a></li>
                <li><a href="#" class="beige" title="Beige"></a></li>
                <li><a href="#" class="purple" title="Purple"></a></li>
                <li><a href="#" class="celadon" title="Celadon"></a></li>
                <li><a href="#" class="red" title="Red"></a></li>
                <li><a href="#" class="brown" title="Brown"></a></li>
                <li><a href="#" class="cherry" title="Cherry"></a></li>
                <li><a href="#" class="cyan" title="Cyan"></a></li>
                <li><a href="#" class="gray" title="Gray"></a></li>
                <li><a href="#" class="olive" title="Olive"></a></li>
            </ul>
        </div>

    </div>
</body>
</html>
