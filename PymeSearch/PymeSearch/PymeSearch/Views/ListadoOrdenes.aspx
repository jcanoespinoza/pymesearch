﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ListadoOrdenes.aspx.cs" Inherits="PymeSearch.Views.ListadoOrdenes" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PymeSearch</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <script src="../Assets/templateStyle/scripts/jquery-3.5.1.min.js"></script>
    <link href="../Assets/templateStyle/css/style.css" rel="stylesheet" />
    <link href="../Assets/templateStyle/css/main-color.css" rel="stylesheet" />
    <script src="../Assets/templateStyle/sweetalert.min.js"></script>
    <script src="../Assets/Js/ListadoOrdenes.js"></script>
</head>
<body>
    <!-- Wrapper -->
    <div id="wrapper">
        <!-- Header Container
================================================== -->
        <header id="header-container">
            <!-- Header -->
            <div id="header">
                <div class="container">

                    <!-- Left Side Content -->
                    <div class="left-side">

                        <!-- Logo -->
                        <div id="logo">
                            <%--<a href="index.html">
                            <img src="images/logo.png" alt=""></a>--%>
                            <h5>PymeSearch</h5>
                        </div>

                        <!-- Mobile Navigation -->
                        <div class="mmenu-trigger">
                            <button class="hamburger hamburger--collapse" type="button">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>

                        <nav id="navigation" class="style-1">
                            <ul id="responsive">
                                <li><a class="current" href="AdministrarComercio.aspx">Administrar comercio</a></li>
                            </ul>
                        </nav>

                        <div class="clearfix"></div>

                    </div>
                    <!-- Right Side Content / End -->
                    <div class="right-side">
                        <!-- Header Widget -->
                        <div class="header-widget">

                            <!-- User Menu -->
                            <div class="user-menu">
                                <div class="user-name">
                                    <span>
                                        <img src="images/dashboard-avatar.jpg" alt="" /></span>Bienvenido, vendedor
                                </div>
                                <ul>
                                    <li><a href="Principal.aspx"><i class="sl sl-icon-power"></i>Cerrar sesión</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- Header Widget / End -->
                    </div>
                    <!-- Right Side Content / End -->
                </div>
            </div>
        </header>

        <div class="clearfix"></div>

        <!-- Titlebar
================================================== -->
        <div id="titlebar" class="gradient">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <h2>Mis ordenes</h2>
                        <span>Listado de ordenes de la tienda</span>
                    </div>
                </div>
            </div>
        </div>


        <!-- Content
================================================== -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">

                        <!-- Listings -->
                        <div class="col-lg-12 col-md-12">
                            <div class="dashboard-list-box margin-top-0">
                                <ul id="listaOrdenes">
                                </ul>
                            </div>
                        </div>
                    </div>

    <!-- Pagination -->
    <div class="clearfix"></div>
    <%--<div class="row">
        <div class="col-md-12">
            <!-- Pagination -->
            <div class="pagination-container margin-top-20 margin-bottom-40">
                <nav class="pagination">
                    <ul>
                        <li><a href="#" class="current-page">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#"><i class="sl sl-icon-arrow-right"></i></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>--%>
    <!-- Pagination / End -->

    </div>

            </div>
        </div>


        <!-- Footer
================================================== -->
    <div id="footer" class="sticky-footer">
        <div class="copyrights">© 2019 Listeo. All Rights Reserved.</div>
    </div>
    <!-- Footer / End -->


    <!-- Back To Top Button -->
    <div id="backtotop"><a href="#"></a></div>


    </div>
    <!-- Wrapper / End -->

    <!-- Scripts
================================================== -->

    <script src="../Assets/templateStyle/scripts/jquery-migrate-3.3.1.min.js"></script>
    <script src="../Assets/templateStyle/scripts/mmenu.min.js"></script>
    <script src="../Assets/templateStyle/scripts/chosen.min.js"></script>
    <script src="../Assets/templateStyle/scripts/slick.min.js"></script>
    <script src="../Assets/templateStyle/scripts/rangeslider.min.js"></script>
    <script src="../Assets/templateStyle/scripts/magnific-popup.min.js"></script>
    <script src="../Assets/templateStyle/scripts/waypoints.min.js"></script>
    <script src="../Assets/templateStyle/scripts/counterup.min.js"></script>
    <script src="../Assets/templateStyle/scripts/jquery-ui.min.js"></script>
    <script src="../Assets/templateStyle/scripts/tooltips.min.js"></script>
    <script src="../Assets/templateStyle/scripts/custom.js"></script>
</body>
</html>
