﻿using Newtonsoft.Json;
using PymeSearch.Data;
using PymeSearch.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PymeSearch.Views
{
    public partial class Principal : System.Web.UI.Page
    {
        [WebMethod(enableSession:true)]
        public static void CrearUsuarioComercio(string usuario, string contrasenna)
        {
            Usuario user = new Usuario();
            user.usuario = usuario;
            user.contrasenna = contrasenna;
            UsuarioDatos.createUser(user);

        }

        [WebMethod(enableSession: true)]
        public static string getUserLogin(string usuario)
        {
            var result=UsuarioDatos.getUserLogin(usuario);
            string JsonString = string.Empty;
            JsonString = JsonConvert.SerializeObject(result);
            return JsonString;
            

        }


    }
}