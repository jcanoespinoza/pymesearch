﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PymeSearch.Entities
{
    public class Promocion
    {
        public int Id { get; set; }
        public int Nombre { get; set; }

        public Item articulo { get; set; }

        public DateTime FechaExpiracion { get; set; }


        public Promocion()
        {
            articulo = new Item();
        }
    }
}