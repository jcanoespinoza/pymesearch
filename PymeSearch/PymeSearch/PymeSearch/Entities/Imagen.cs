﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PymeSearch.Entities
{
    public class Imagen
    {
        public int Id { get; set; }
        public Byte[] imagen { get; set; }

    }
}