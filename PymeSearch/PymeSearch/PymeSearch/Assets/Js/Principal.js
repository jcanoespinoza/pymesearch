﻿var APP = window.APP || {}

APP.Principal = function () {
    var UbicacionActual;
    var map;
    var lastMarkers = [];
    var init = function () {
        console.log('Principal');
        inicializarComponentes();
    }
    var inicializarCoordenadas = function (lat, lon) {
        map = L.map('map', { center: [lat, lon], zoom: 15, maxZoom: 18, minZoom: 3 })
        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>'
        }).addTo(map);
        L.control.scale().addTo(map);

        UbicacionActual = L.marker([lat, lon], { draggable: true }).addTo(map);
        UbicacionActual.bindPopup("<b>Estas aquí</b><br>");
    }

    var inside = function (point, geocerca) {

        var x = point[0], y = point[1];

        var inside = false;
        for (var i = 0, j = geocerca.length - 1; i < geocerca.length; j = i++) {
            var xi = geocerca[i][0], yi = geocerca[i][1];
            var xj = geocerca[j][0], yj = geocerca[j][1];

            var intersect = ((yi > y) != (yj > y))
                && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
            if (intersect) inside = !inside;
        }

        return inside;
    };

    var crearGeocerca = function () {

        var arregloCoordenadas =
            [
                {
                    latitud: 9.98686,
                    longitud: -84.2475
                },
                {
                    latitud: 9.99109,
                    longitud: -84.23754
                },
                {
                    latitud: 9.98399,
                    longitud: -84.23325
                },
                {
                    latitud: 9.93639,
                    longitud: -84.10481
                }
            ];

        var latitud = UbicacionActual._latlng.lat;
        var longitud = UbicacionActual._latlng.lng;

        if (latitud > 0) {
            arriba = latitud + 0.05;
            abajo = latitud - 0.05;
        }
        else {
            arriba = latitud - 0.05;
            abajo = latitud + 0.05;
        }

        if (longitud > 0) {
            izquierda = longitud + 0.05;
            derecha = longitud - 0.05;
        }
        else {
            izquierda = longitud - 0.05;
            derecha = longitud + 0.05;
        }

        var polygon = [[parseFloat(arriba), parseFloat(derecha)],
        [parseFloat(abajo), parseFloat(derecha)],
        [parseFloat(abajo), parseFloat(izquierda)],
        [parseFloat(arriba), parseFloat(izquierda)]];

        for (var i = 0; i < arregloCoordenadas.length; i++) {
            if (inside([arregloCoordenadas[i].latitud, arregloCoordenadas[i].longitud], polygon)) {
                ComercioEncontrado = L.marker([arregloCoordenadas[i].latitud, arregloCoordenadas[i].longitud], { draggable: false }).addTo(map);
                ComercioEncontrado.bindPopup("<b>Comercio encontrado #" + (i + 1) + "</b><br><a href='ResultadoBusqueda.aspx'>Más información</a>");
                lastMarkers.push(ComercioEncontrado);
            }
        }

        if (lastMarkers.length < 1) {
            alert('No se han encontrado comercios en tu busqueda');
        }
    }

    var realizarBusqueda = function () {
        var comercioBusqueda = document.getElementById('txtBusqueda').value;
        var ubicacionBusqueda = document.getElementById('autocomplete-input').value;

        if (lastMarkers.length > 0) {
            for (var i = 0; i < lastMarkers.length; i++) {
                map.removeLayer(lastMarkers[i]);
            }
            lastMarkers = [];
        }

        if (comercioBusqueda === '' && ubicacionBusqueda === '') {
            alert('Debe completar los campos necesarios para realizar la busqueda');
        }
        else {
            crearGeocerca();
        }
    }

    var iniciarSesion = function () {
        var usuario = $('#username').val();
        var contrasenna = $('#password').val();

        if (validarVacios('Ingresar') == true) {
            $.ajax({
                type: "POST",
                url: "../Views/Principal.aspx/getUserLogin",
                dataType: "json",
                data: JSON.stringify({
                    usuario: usuario
                }),
                contentType: "application/json; charset=utf-8",
                success: function (response) {

                    var data = JSON.parse(response.d);
                    if (usuario === data.usuario && contrasenna === data.contrasenna) {
                        window.location.replace("../Views/ListadoOrdenes.aspx");

                    }

                },
                failure: function (response) {
                    swal("Error al guardar el registro" + response, "", "error");
                },
                error: function (response) {
                    swal("Error al guardar el registro" + response, "", "error");
                }
            });
        }

       
    }
    var inicializarComponentes = function () {

        //inicializarCoordenadas();
        obtenerGeolocalizacion();
    }
    var obtenerGeolocalizacion = function () {


        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (objPosition) {
                var lon = objPosition.coords.longitude;
                var lat = objPosition.coords.latitude;
                inicializarCoordenadas(lat, lon);
                //content.innerHTML = "<p><strong>Latitud:</strong> " + lat + "</p><p><strong>Longitud:</strong> " + lon + "</p>";

            }, function (objPositionError) {
                switch (objPositionError.code) {
                    case objPositionError.PERMISSION_DENIED:
                        //content.innerHTML = "No se ha permitido el acceso a la posición del usuario.";
                        break;
                    case objPositionError.POSITION_UNAVAILABLE:
                        // content.innerHTML = "No se ha podido acceder a la información de su posición.";
                        break;
                    case objPositionError.TIMEOUT:
                        //content.innerHTML = "El servicio ha tardado demasiado tiempo en responder.";
                        break;
                    default:
                    //content.innerHTML = "Error desconocido.";
                }
            }, {
                maximumAge: 75000,
                timeout: 15000
            });
        }
        else {
            //content.innerHTML = "Su navegador no soporta la API de geolocalización.";
        }
    }
    var CrearUsuarioComercio = function () {
        var usuario = $('#userEmail').val();
        var contrasenna = $('#userPassword').val();
        $.ajax({
            type: "POST",
            url: "../Views/Principal.aspx/CrearUsuarioComercio",
            dataType: "json",
            data: JSON.stringify({
                usuario: usuario,
                contrasenna: contrasenna

            }),
            contentType: "application/json; charset=utf-8",
            success: function (response) {

                swal("Registro guardado correctamente", "", "success");
            },
            failure: function (response) {
                swal("Error al guardar el registro", "", "error");
            },
            error: function (response) {
                swal("Error al guardar el registro", "", "error");
            }
        });
    }
    var validarVacios = function (form) {
        var elements; 
        $("#"+form).each(function () {
            elements = $(this).find(':input') //<-- Should return all input elements in that specific form.
        });
        var resultado=true;
        for (var i = 0; i < elements.length; i++) {
            elements[i].oninvalid = function (e) {
                e.target.setCustomValidity("");
                if (!e.target.validity.valid) {
                    e.target.setCustomValidity("Este campo no puede estar vacío");
                     resultado= false;
                }
            };
            elements[i].oninput = function (e) {
                e.target.setCustomValidity("");
            };
        }
        return resultado;
    }
    
    return {
        init: init,
        realizarBusqueda: realizarBusqueda,
        iniciarSesion: iniciarSesion,
        CrearUsuarioComercio: CrearUsuarioComercio
    }
}();
$(document).ready(function () {
    APP.Principal.init();
});