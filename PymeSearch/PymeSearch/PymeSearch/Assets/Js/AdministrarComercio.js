﻿var APP = window.APP || {}

APP.AdministrarComercio = function () {
    var MapaAdministracionComercio;

    var init = function () {
        console.log('AdministrarComercio');
        inicializarComponentes();
    }

    var inicializarCoordenadas = function (lat, lon) {
        MapaAdministracionComercio = L.map('MapaAdministracionComercio', { center: [lat, lon], zoom: 15, maxZoom: 18, minZoom: 3,zoomControl:false })
        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>'
        }).addTo(MapaAdministracionComercio);
        L.control.scale().addTo(MapaAdministracionComercio);
        swal("¡La Rutina se guardó correctamente!", "Rutina", "success");
        var UbicacionActual = L.marker([lat, lon], { draggable: true }).addTo(MapaAdministracionComercio);
        UbicacionActual.bindPopup("<b>Tu comercio se encuentra aquí</b><br>");
    }
    var inicializarComponentes = function () {
        obtenerGeolocalizacion();
    }
    var obtenerGeolocalizacion = function () {


        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (objPosition) {
                var lon = objPosition.coords.longitude;
                var lat = objPosition.coords.latitude;
                inicializarCoordenadas(lat, lon);
                //content.innerHTML = "<p><strong>Latitud:</strong> " + lat + "</p><p><strong>Longitud:</strong> " + lon + "</p>";

            }, function (objPositionError) {
                switch (objPositionError.code) {
                    case objPositionError.PERMISSION_DENIED:
                        //content.innerHTML = "No se ha permitido el acceso a la posición del usuario.";
                        break;
                    case objPositionError.POSITION_UNAVAILABLE:
                        // content.innerHTML = "No se ha podido acceder a la información de su posición.";
                        break;
                    case objPositionError.TIMEOUT:
                        //content.innerHTML = "El servicio ha tardado demasiado tiempo en responder.";
                        break;
                    default:
                    //content.innerHTML = "Error desconocido.";
                }
            }, {
                maximumAge: 75000,
                timeout: 15000
            });
        }
        else {
            //content.innerHTML = "Su navegador no soporta la API de geolocalización.";
        }
    }
    var validarCamposVacios = function () {

    }
    var guardarcomercio = function () {

    }
    var limpiar = function () {

    }
    return {
        init: init
    }
}();
$(document).ready(function () {
    APP.AdministrarComercio.init();
});